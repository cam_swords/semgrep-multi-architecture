package main

import (
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func TestBuildArgs(t *testing.T) {
	tests := []struct {
		name          string
		excludedPaths string
		enableMetrics bool
		passedCLIOpts string
		setupFn       func()
		want          []string
	}{
		{
			name:          "Empty Exclude",
			excludedPaths: "",
			enableMetrics: false,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "off",
			},
		},
		{
			name:          "Empty Exclude with whitespace",
			excludedPaths: "  ",
			enableMetrics: false,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "off",
			},
		},
		{
			name:          "Default Exclude",
			excludedPaths: "spec,test,tests,tmp",
			enableMetrics: false,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--exclude", "spec",
				"--exclude", "test",
				"--exclude", "tests",
				"--exclude", "tmp",
				"--metrics", "off",
			},
		},
		{
			name:          "Glob Exclude",
			excludedPaths: "test/*.js,*.rb",
			enableMetrics: false,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--exclude", "test/*.js",
				"--exclude", "*.rb",
				"--metrics", "off",
			},
		},
		{
			name:          "Exclude with whitespace",
			excludedPaths: "spec, test, tests, tmp",
			enableMetrics: false,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--exclude", "spec",
				"--exclude", "test",
				"--exclude", "tests",
				"--exclude", "tmp",
				"--metrics", "off",
			},
		},
		{
			name:          "Enabled Metrics",
			excludedPaths: "",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
			},
		},
		{
			name:          "CLI Opts: append value to flag name with = separator",
			excludedPaths: "",
			passedCLIOpts: "--max-memory=1024",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-memory", "1024",
			},
		},
		{
			name:          "CLI Opts: Include both allowed and not-allowed flag with = separator",
			excludedPaths: "",
			passedCLIOpts: "--max-memory=1024 --optimizations=all", // invalid
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-memory", "1024",
			},
		},
		{
			name:          "CLI Opts: Include only allowed flag with space separator",
			excludedPaths: "",
			passedCLIOpts: "--max-memory 1024",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-memory", "1024",
			},
		},
		{
			name:          "CLI Opts: Include both allowed and not-allowed flag with space separator",
			excludedPaths: "",
			passedCLIOpts: "--max-memory 1024 --optimizations all",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-memory", "1024",
			},
		},
		{
			name:          "CLI Opts: Include only not-allowed flag",
			excludedPaths: "",
			passedCLIOpts: "--optimizations=all",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
			},
		},
		{
			name:          "CLI Opts: Include only --max-target-bytes flag with = separator",
			excludedPaths: "",
			passedCLIOpts: "--max-target-bytes=5000",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-target-bytes", "5000",
			},
		},
		{
			name:          "CLI Opts: Include --max-target-bytes flag with space separator",
			excludedPaths: "",
			passedCLIOpts: "--max-target-bytes 5000",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-target-bytes", "5000",
			},
		},
		{
			name:          "CLI Opts: Include both --max-target-bytes and --max-memory flags",
			excludedPaths: "",
			passedCLIOpts: "--max-target-bytes=5000 --max-memory=2048",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-target-bytes", "5000",
				"--max-memory", "2048",
			},
		},
		{
			name:          "CLI Opts: Include only --timeout flag with = operator",
			excludedPaths: "",
			passedCLIOpts: "--timeout=300",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--timeout", "300",
			},
		},
		{
			name:          "CLI Opts: Include --timeout flag with space seperator",
			excludedPaths: "",
			passedCLIOpts: "--timeout 300",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--timeout", "300",
			},
		},
		{
			name:          "CLI Opts: Invalid flag with --max-target-bytes and --max-memory",
			excludedPaths: "",
			passedCLIOpts: "--max-target-bytes=5000 --max-memory=2048 --optimizations=all", // invalid
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--max-target-bytes", "5000",
				"--max-memory", "2048",
			},
		},
		{
			name:          "CLI Opts: Include all --timeout, --max-memory, and --max-target-bytes flags",
			excludedPaths: "",
			passedCLIOpts: "--timeout=300 --max-memory=2048 --max-target-bytes=5000",
			enableMetrics: true,
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "on",
				"--timeout", "300",
				"--max-memory", "2048",
				"--max-target-bytes", "5000",
			},
		},
		{
			name:    "passes the --verbose flag when log level is debug",
			setupFn: func() { log.SetLevel(log.DebugLevel) },
			want: []string{
				"-f", "configPath",
				"-o", "outputPath",
				"--sarif",
				"--no-rewrite-rule-ids",
				"--strict",
				"--disable-version-check",
				"--no-git-ignore",
				"--metrics", "off",
				"--verbose",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.setupFn != nil {
				tt.setupFn()
			}

			got := buildArgs("configPath", "outputPath", tt.excludedPaths, tt.passedCLIOpts, tt.enableMetrics)
			require.Equal(t, tt.want, got)
		})
	}
}
