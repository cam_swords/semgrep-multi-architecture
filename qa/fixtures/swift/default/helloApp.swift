//
//  helloApp.swift
//  hello
//

import SwiftUI
import LocalAuthentication

@main
struct helloApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: "username",
            kSecValueData as String: "password".data(using: .utf8)!,
            //ruleid:rules_lgpl_swift_other_rule-ios-keychain-weak-accessibility-value
            kSecAttrAccessible as String: kSecAttrAccessibleAlways
        ]
    }
}
