//
//  main.m
//  app
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    
    BOOL allowsExpiredCertificates = NO; // This is set according to your requirements
    BOOL allowsAnyRoot = NO; // This is set according to your requirements
    BOOL allowsExpiredRoots = NO; // This is set according to your requirements
    //ruleid: rules_lgpl_oc_other_rule-ios-self-signed-ssl
    BOOL validatesSecureCertificate = NO; // This is set according to your requirements
    //ruleid: rules_lgpl_oc_other_rule-ios-self-signed-ssl
    BOOL allowInvalidCertificates = YES; // This is set according to your requirements
    
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
